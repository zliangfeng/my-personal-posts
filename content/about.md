---
title: "务工记"
date: "2019-03-23"
---

*latest update at 2020/04/01

[resume 在此](/my-resume)

本人可是科班出身的BIT（Bioinformatic IT），然而前厂项目组ERP项目做到一半的时候，开发渐渐跑路，吾等PERLer鬼使神差地被抓去写Java/JSP，半路出家做起了FE！

虽说大约在2011年，就开始接触 jquery，但直到13年开始写 jsp，才开始写jquery；刚用jquery的那会，可谓一把梭一时爽，但一直用却并不爽！毕竟面对几百上千行的 $("selector")时，大佬也会腿软。

写了一年多的 jsp / jquery，我决定试试FE的框架。那会可选的框架挺多，angular，ember，backbone云云。过了这么久，我也忘了是怎么搜到reactjs，不过，从看到reactjs的代码demo起，就感觉发现了一块新大陆：原来前端还TM的可以这样写。

刚写react的那会，react的版本还是0.XX，而我竟然有小半年的时间代码都直接扔到<script>里面。做法虽low，但开发的效率却与日俱增，嗯，sorry，写 raw code的时光，再也回不去了！

人生有很多milestone，我的FE生涯也是，如：

\- create-react-app

\- react-bootstrap

\- redux

如今，使用reactjs已经4年，reactjs目前已经到了使用hooks的版本！而最近要搬的砖比较多，我竟没时间去试试，好可惜！

至于目前主流的其他框架，比如vue，angular等，我应该是不会去研究的，毕竟我是曾经写过jsp/jquery的，那些在html元素上写for/if的方式，sorry，无感了！

还是老老实实写react吧，毕竟以后弃坑了，我还是一个JSer，还可以转去写nodejs，做backend/fullstack啥的。

至于什么时候弃坑呢，等写完《前端脱坑指北》吧！
