## **风

Web前端开发

-   手机：(+86) 136 **** 5059
-   邮箱：<zliangfeng@gmail.com>
-   网站：[lfzhou.ml](https://lfzhou.ml)
-   GitHub: [github.com/zliangfeng](https://github.com/zliangfeng)

------

### 简介

- 日常开发以 JS 为主，自2015年接触 ReactJS 以来，该框架一直为项目中前端开发的主力；后端使用 NodeJS + ExpressJS 等.
- 从事过 3 年的 JavaEE 开发，以 SpringMVC & Hibernate 入门，后逐渐采用 SpringBoot；前端使用的是 jQuery，后弃用.
- 曾写过两年的 Perl/Bash 脚本，主要用于数据分析以及生成电子报告.
- 英文能力不错，可熟练阅读文档以及前沿技术 Posts.

------

### 工作经历

#### *软件工程师 at 深圳鼎新融合科技有限公司, 深圳*

2016-08 至今

-   作为公司IT负责人，开发ERP系统、企业公众号和官网，以及搭建维护办公系统（wiki，samba等），提高生产自动化水平及办公效率.
-   以个人生物信息学的背景，为公司消费级基因检测产品提供建议和产品研发支持；独立开发出基因型判读系统，准确度99%.
-    为合作方“雄伟科技”的“智盘”系统开发自动排餐/营养分析的 API.

#### *软件开发 at 华大基因, 深圳*

2011-07 至 2016-08

-   自动化生产系统（EasyGenomics）开发，包括开发数据分析Web平台，整合后端 SGE 集群调度系统，以及移植数据分析流程.
-   维护升级smallRNA分析流程，对测序数据调用相关软件（Linux command line software）及编写算法进行数据分析.
-   日常维护第二代测序仪测序数据（rawdata）生产、交付系统.

------

### 教育背景

#### *华中科技大学 - 生物信息技术 / 工学学士*

2007-09 至 2011-06

------

### 技能

#### *编程语言*
-  ★★★★☆ JavaScript，熟练
-  ★★★☆☆ Java，熟悉
-  ★★★★☆ Perl，熟练
-  ★★★☆☆ Bash，熟悉

#### *模板标记语言（Markup/Templating Languages）*

-   ★★★★☆ HTML，熟练
-   ★★★★☆ CSS，熟练
-   ★★★☆☆ SASS，熟悉
-   ★★★☆☆ Markdown，熟悉

#### 框架

-   ★★★★☆ ReactJS，熟练
-   ★★★☆☆ ExpressJS，熟悉
-   ★★★★☆ NodeJS，熟练
-   ★★★☆☆ SpringBoot，熟悉

#### Software & Tools

-   Git
-   MySQL
-   MacOS / Ubuntu Linux
-   Vim
