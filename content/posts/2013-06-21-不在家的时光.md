---
title: "不在家的时光"
date: "2013-06-21"
---
```
今夜我不在家， 窗棱的微尘多了几许， 秋寒渐甚， 我的花儿瘦成了音符。

书签留在翻到的一页， 收拾过的纸篓依旧空空， 桌上的杯子也没动， 只有风帘处渗透着清风。

墙上的挂历是我离开时候的颜色， 相框里的人儿笑容依旧， 偶尔有小生灵来访， 却没能发生照面。

我不在家的时光， 有三百五十五天之长， 时钟在静止， 而我在流逝。
```
