---
title: "Listening Original Soundtrack"
date: "2018-05-07"
---
```
余音绕梁，
亦真亦幻，
如沐春风，
如饮甘泉，
如瞰墨林，
如步秋原，
如临深渊，
如上云端，
 
如是梦一场。
```
