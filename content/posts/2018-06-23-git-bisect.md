---
title: "Git Bisect"
date: "2018-06-23"
---

额，N年前苦逼的大学僧学算法时代，学到的二分法竟然在这里用到了；

二分法不难理解，这里使用 git bisect + bash 实现自动查找出 bug 的 commit；

- git bisect start bad-commit-id good-commit-id #启动二分法查找的范围，eg: git bisect start HEAD v1.0
- 写一个测试的 bash 脚本 test-error.sh，good 的情况 exit 0; bad 的情况 exit 1;
- git bisect run sh test-error.sh # 运行，等待。。。

真是个不错的技能！！
