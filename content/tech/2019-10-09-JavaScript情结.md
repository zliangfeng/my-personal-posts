---
title: "JavaScript情结"
date: "2019-10-09"
---

original ref: https://flaviocopes.com/solve-javascript-fatigue/

以下是一家之言，听之任之：

- 作为脚本语言，JS是很灵活的，如Perl/Bash；然后，爱之深也恨之切，有很多吃瘪的设计；如果真正使用的时候，尽量参考《JavaScript: the good parts》一书；
- JavaScript is not Java;
- NodeJS + React/Vue/Angular，BE + FE，算是做 fullstack 的一种捷径吧；
- Sometimes learning in 20% of the time the 80% of the things you will need is enough, without diving too much into the edge cases.