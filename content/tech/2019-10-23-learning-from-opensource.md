---
title: "从开源项目偷师技术"
date: "2019-10-23"
---

original ref: https://twitter.com/laisky_sh/status/1186660408054054912

- redis ==> 数据结构
- elasticsearch & kafka ==> 文件存储设计
- net ==> 高性能的网络服务器设计
- spring ==> IoC 的设计思想
- etcd ==> raft 的实践细节
- kubernetes ==> 声明式服务设计
