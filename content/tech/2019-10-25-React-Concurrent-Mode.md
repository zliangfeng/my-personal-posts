---
title: "React Concurrent Mode"
date: "2019-10-25"
---

original ref: https://reactjs.org/docs/concurrent-mode-patterns.html

### **动画**

**适量**流畅的动画可以改善UX（User Experience）

- data remote loading 造成的spinner，场景包括
  - 页面A切换到B
  - 按钮点击
- 点击/切换
  - 切换如果在极短的时间加载完成，则不需要一个blank（！！为了用户体验，因为这里出现A-blank-B三个画面）
  - 如果超过设定的时长，则需要indication（这里是spinner）页面过渡

### Recap

The most important things we learned so far are:

- By default, our loading sequence is Receded → Skeleton → Complete.
- The Receded state doesn’t feel very nice because it hides existing content.
- With `useTransition`, we can opt into showing a Pending state first instead. This will keep us on the previous screen while the next screen is being prepared.
- If we don’t want some component to delay the transition, we can wrap it in its own `<Suspense>` boundary.
- Instead of doing `useTransition` in every other component, we can build it into our design system.