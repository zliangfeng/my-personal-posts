---
title: "创建Gorgeous UI的七原则"
date: "2019-11-19"
---

original ref: 

1. https://medium.com/@erikdkennedy/7-rules-for-creating-gorgeous-ui-part-1-559d4e805cda
2. https://medium.com/@erikdkennedy/7-rules-for-creating-gorgeous-ui-part-2-430de537ba96

- **Light comes from the sky**

  - 对于流行了5年多的 flat design，作者认为 flatty design （ some shadows,  balance between clean look and intuitive appearance.）会是新趋势（按：flat design 没有光影效果，以side view的角度去看，就是平板一块，Often suffer usability issues ！！！）

- **Black and white first**

  - Add color last, and even then, only with purpose.
  - 推荐用 hsb 替换 rgb （？？额，浏览器默认是hsl）

- **Double your whitespace**

  - If you want to make UI that looks designed, you need to add in a lot of breathing room.

  - Put space between your lines.

    Put space between your elements.

    Put space between your groups of elements.

    **Analyze what works**.

- **Learn the methods of overlaying text on images** 

  - Method 0: Apply text directly to image ｜直接放文字
  - Method 1: Overlay the whole image ｜覆盖大图片
  - Method 2: Text-in-a-box ｜文字加深色背景 box
  - Method 3: Blur the image ｜虚化图片（文字背景层虚化 or 图片本身虚化）
  - Method 4: Floor fade ｜文字加向下阴影
  - Bonus Method: Scrim ｜蒙版（A scrim is a piece of photography equipment that makes light softer）

- **Make text pop — and un-pop**

  - **Styles that increase visibility of the text**. Big, bold, capitalized, etc.
  - **Styles that decrease visibility of the text**. Small, less contrast, less margin, etc.

- **Only use good fonts** 

  - https://feathericons.com/

- **Steal like an artist**

  - https://www.pinterest.com/warmarc/ui-design/