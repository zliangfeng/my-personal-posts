---
title: "TCP 三次握手 & HTTP 协议"
date: "2019-11-20"
---

original ref: https://draveness.me/whys-the-design-tcp-three-way-handshake

### **Why and How**

TCP 连接使用三次握手的首要原因 —— 为了阻止历史的**重复连接初始化**造成的混乱问题。（防止使用 TCP 协议通信的双方建立了错误的连接）

建立 TCP 连接，即通信的双方达成三种信息共识：

1. Sockets，eg: ip/domain + port
2. Window size, 用作流控制
3. Sequence Number, 用来追踪通信发起方发送的数据包序号

TCP message body：

- CTL（flow control），包括 SYN（？？synchronous？），ACK(acknowledge），RST(reset)

- SEQ

  

### **TCP/A  <=> TCP/B**

=> { SEQ: 100, CTL: SYN }（向B发请求）

<= { ACK: 101, CTL: ACK }（回应A的请求）and { SEQ: 300, CTL: SYN } (向A请求，可以建立连接)

=> { SEQ: 301, CTL: ACK or RST } // 知识点：由请求发起者决定**建立connection，或者reset**

```mermaid
sequenceDiagram
		participant A as Client
		participant B as Server
		A->>+B: SEQ:100,CTL:SYN / 1
		B-->>-A: ACK:101,CTL:ACK / 2
		Note over A,B: 回应A的请求 and 向A请求，可以建立连接
		B-->>A: SEQ:300,CTL:SYN / 2
		alt 继续建立连接
		A->>B: SEQ:301,CTL:ACK / 3
		else 终止连接
		A->>B: SEQ:301,CTL:RST / 3
		end
```
{{<mermaid>}}
sequenceDiagram
		participant A as Client
		participant B as Server
		A->>+B: SEQ:100,CTL:SYN / 1
		B-->>-A: ACK:101,CTL:ACK / 2
		Note over A,B: 回应A的请求 and 向A请求，可以建立连接
		B-->>A: SEQ:300,CTL:SYN / 2
		alt 继续建立连接
		A->>B: SEQ:301,CTL:ACK / 3
		else 终止连接
		A->>B: SEQ:301,CTL:RST / 3
		end
{{</mermaid>}}



### HTTP/1.* & 2 & 3

#### cheatsheet

|                                          | 1.0                                           | 1.1                                                          | 2 （from SPDY 协议）                                         | 3 （原称HTTP-over-QUIC）                                     |
| ---------------------------------------- | --------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 连接复用                                 | ❌每次都需要重新建立连接，增加延迟             | 加入 keep-alive 可以复用一部分，但域名分片等情况下仍然需要建立多个 connection | 同域名下所有通信都在单个连接上完成；**多路复用**；（但如果发生了丢包，单连接特性导致整个 TCP 都要开始等待重传，此时性能不如 HTTP/1.1） | QUIC 基于 UDP，一个连接上的多个 stream 之间没有依赖（丢包不影响其他stream） |
| 队头阻塞 ｜Head-Of-Line Blocking（HOLB） | ❌下个请求必须在前一个请求返回后才能发出       | 尝试使用 pipeling 来解决，但 pipeling 要求返回是按序的，并且如果同一域名链接数超过限制，则会被挂起等待一段时间 | 单个连接可以承载任意数量的双向数据流；帧之间可以乱序发送，再根据帧首部的流标识可以重新组装 | UDP无序，所以**不会发生HOLB**                                |
| 协议开销大                               | ❌header 里携带的内容过大                      | 同1.0                                                        | 客户端和服务器端使用在连接存续期内始终存在的“**首部表**”来跟踪和存储之前发送的键－值对，新的首部键－值对被追加/替换（增量更新）；服务端能通过 **Server Push** 的方式将客户端需要的内容预先推送过去 |                                                              |
| 安全因素                                 | ❌明文传输，TCP 协议头部没有经过任何加密和认证 | 同1.0                                                        | 请求和响应数据分割为更小的帧，并且它们采用**二进制编码**     | QUIC 的 packet，除了个别报文比如 PUBLIC_RESET 和 CHLO，所有报文头部都是经过认证的，报文 Body 都是经过加密的。**向前纠错机制**：添加校验包，如果丢一个包，可以通过其他包校验找回；多于1个就要重传了 |

#### HTTP2 重要概念：

-   流：流是连接中的一个虚拟信道，可以承载双向的消息；每个流都有一个唯一的整数标识符（1、2…N）；
-   消息：是指逻辑上的 HTTP 消息，比如请求、响应等，由一或多个帧组成。多个帧之间可以乱序发送，根据帧首部的流标识可以重新组装。
-   帧：HTTP 2.0 通信的最小单位，每个帧包含帧首部，至少也会标识出当前帧所属的流，承载着特定类型的数据，如 HTTP 首部、负荷，等等
-   在 HTTP/2 中，每个请求都可以带一个 **31bit 的优先值**，0 表示最高优先级， 数值越大优先级越低。有了这个优先值，客户端和服务器就可以在处理不同的流时采取不同的策略，以最优的方式发送流、消息和帧。

![HTTP2](https://tva1.sinaimg.cn/large/007S8ZIlgy1ggsnban551j30g20d3dhm.jpg)



####  QUIC 新功能

-   0-RTT

通过使用类似 TCP 快速打开的技术，缓存当前会话的上下文，在下次恢复会话的时候，只需要将之前的缓存传递给服务端验证通过就可以进行传输了。**0RTT 建连可以说是 QUIC 相比 HTTP2 最大的性能优势**。那什么是 0RTT 建连呢？

这里面有两层含义:

-   传输层 0RTT 就能建立连接。
-   加密层 0RTT 就能建立加密连接。

#### 总结：

-   HTTP/1.x 有连接无法复用、队头阻塞、协议开销大和安全因素等多个缺陷
-   HTTP/2 通过多路复用、二进制流、Header 压缩等等技术，极大地提高了性能，但是还是存在着问题的
-   QUIC 基于 UDP 实现，是 HTTP/3 中的底层支撑协议，该协议基于 UDP，又取了 TCP 中的精华，实现了即快又可靠的协议

### **开放问题**

- 除了使用序列号是否还有其他方式保证消息的不重不丢？

  A：只要是有序的就行吧！相对而言，序列号是最简单的；

- UDP 协议有连接的概念么，它能保证数据传输的可靠么？

  A: 无连接协议，不能保证数据可靠