---
title: "20 ways to become a better Node.js developer in 2020"
date: "2019-12-16"
---

original ref: https://medium.com/@me_37286/20-ways-to-become-a-better-node-js-developer-in-2020-d6bd73fcf424

### 1. 使用TypeScript

切换到新的语言毕竟是有代价的，需要一段时间才能摸索出一套 best practice，根据 https://medium.com/javascript-scene/the-typescript-tax-132ff4cb175b 文章，大型项目切换到ts并不能带来非常高的收益；小项目可以切换到ts，方便与其他 ts 项目交流；

使用 ts 与否，我摇摆了很久：
-   我不是很喜欢 Java，复杂的 generic 常常让人看着蒙B；
-   早期曾试用过一段时间，体验并不好。ps：试写一段代码，加 type 与否，转译出的 js 代码是一样的，what？
-   设计/规范能规避很多的 bug

>   -  设计和规范评审（最高可减少 80％的 bug）；
>   -   TDD（剩余 bug 再减少 40％到 80％）；
>   -   代码评审（一小时的代码评审可节省 33 小时的维护时间）。

**2020-01-15 choice: 继续观望**

### 2. 使用现代化的测试工具
Jest已经在用作Unit Test；剩下的是如何搭建 Integration/Component Test  & Funtional Test

### 3. 使用 ES6 Modules
done

### 4. 使用最新的 JS 特性
- optional chaining: foo?.bar 的写法，now 使用 babel 可以实现
- private methods and fields: 你们闹哪样！
- ...

### 5. GraphQL
试用过，继续观望

### 6. 使用 Nest.js
Nest aims to provide an application architecture out of the box which allows for effortless creation of highly testable, scalable, loosely coupled and easily maintainable applications.
能 have a shot to typescript & fastify. 有时间试试
** 2020-01-15: AOP，OOP，FP，额，这货就是抄袭 spring?? 我都用上 JS 了，还要用 Spring Style?? **

### 7. 灰度测试技术？？
原文为：Apply gradual deployment techniques like feature-flagging, canary or traffic shadowing。
- feature-flagging：开启特性
- canary or traffic shadowing：测试版本

### 8. 测试，越早越好，越多越好
yes

### 9. 在 production 中测试
使用:
- traffic shadowing: 
    - canary (金丝雀，那只为瓦斯工人探测空气的鸟) 测试，部署少量的资源，主要用于测试版本的特性/bug, 比如 chrome
    - GreenBlueTesting：部署的资源一半一半吧，用于上线切换，发现 bug 可以及时回滚
- a/b test：主要用于测试客户的偏好性
- load testing：负载测试
- tap-compare: 回归测试？？
- ...

### 10. 使用 worker threads
观望中，如果能与父进程共享对象就好了（据说可以实现）

### 11. Deepen your Docker and Kubernetes understanding. It highly affects your Node.js code.
// TODO

### 12. 安全
- 使用 linter security rules
- 使用中间件限制并发访问数（nginx, rate-limiter-flexible, express-rate-limit ...）
- 将 secrets 加密发布
```js
// 该 package 的作者申明不要用作 password 的加密，only use `bcrypt` 
// 这个方法的核心是发布加密后的 encryptedToken，将 secret 写入到 env 中
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.SECRET);
const encryptedToken = cryptr.encrypt('accessToken, keep it in secret.'); // run locally

export const accessToken = cryptr.decrypt(encryptedToken);
```
- 修改 HTTP responce header，如使用 helmet.js
- 使用 JSON schemas
- 给 token 设置 expired date 和 blacklist
- 限制同一IP每日错误密码验证次数，防止暴力破解
- ...

### 13. Monitoring
// TODO ELK or Prometheus

### 14. 使用机器学习作为 black-box product
使用场景：
- recommending things to the user
- classify things
- Find similarities between texts
- predict
- analyze audio or image
- ...

### 15. 睡足 >= 7 hrs
// aha

### 16. 用 Fastify and Koa 替换 Express
额，因为Express好久不更新了？？
** 2020-01-16: 暂时不替换吧 **

### 17. 仍然实用的
- Linux 
- NodeJS Skill

### 18. CI/CD
// TODO 需要将CI/CD分开探讨，CI关注点是按策略自动拉取并测试；而CD将合适的（例如特定tag的）版本部署

### 19. 丰富观念，多样化工具｜Enrich your mindset, diversify your toolbox
### 20. 样板项目