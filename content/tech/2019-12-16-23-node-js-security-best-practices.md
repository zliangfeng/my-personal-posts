---
title: "最佳实践：23+ Node.js security"
date: "2019-12-16"
---

original ref: https://medium.com/@nodepractices/were-under-attack-23-node-js-security-best-practices-e33c146cb87d

安全一个很广泛的话题，一两篇文章显然不能说清；这里从原来23种，列举了几个可实践的建议：

1. 使用 linter security rules
2. 使用中间件限制并发访问数（nginx, rate-limiter-flexible, express-rate-limit ...）
3. 将 secrets 加密发布
```js
// 该 package 的作者申明不要用作 password 的加密，only use `bcrypt` 
// 这个方法的核心是发布加密后的 encryptedToken，将 secret 写入到 env 中
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.SECRET);
const encryptedToken = cryptr.encrypt('accessToken, keep it in secret.'); // run locally

export const accessToken = cryptr.decrypt(encryptedToken);
```

4. 修改 HTTP responce header，如使用 helmet.js
5. 使用 JSON schemas
6. 给 token 设置 expired date 和 blacklist
7. 限制同一IP每日错误密码验证次数，防止暴力破解
8. ...