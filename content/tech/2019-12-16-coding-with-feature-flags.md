---
title: "最佳实践：使用feature flags编码"
date: "2019-12-16"
---

original ref: https://medium.com/@thysniu/coding-with-feature-flags-how-to-guide-and-best-practices-3f9637f51265

### 1. Feature Flags 的类型
- Release toggles
- Operational toggles：主要用于  control flow
- Experimental Toggles
- Permission Toggles

### 2. 一点思考：如何摆脱 if/else
- 暂时没有什么好方案，能做的让某些类的 ff 短暂存在，及时清理
- 读取配置文件
```js
if ($cfg.enable_unicorn_polo) {
    // do something new and amazing here.
}
else {
    // do the current boring stuff.
}
```