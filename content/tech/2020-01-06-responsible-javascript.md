---
title: "前端优化（1）：Responsible Javascript"
date: "2020-01-06"
---

original ref: 

-   https://alistapart.com/article/responsible-javascript-part-1
-   https://calendar.perfplanet.com/2018/doing-differential-serving-in-2019/ （针对 modern/legecy browser，加载ES6+/ES5 js包）

前端开发的时候，随便一个页面加载的js就可能达到400+K，浏览器需要2+秒的时间去parse；本文试图从各个方面怼这个问题（performance and accessibility）。

## Don’t let frameworks force you into unsustainable patterns

```javascript
// 反面例子
import React, { Component } from "react";
import { validateEmail } from "helpers/validation";

class SignupForm extends Component {
  constructor (props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateEmail = this.updateEmail.bind(this);
    this.state.email = "";
  }

  updateEmail (event) {
    this.setState({
      email: event.target.value
    });
  }

  handleSubmit () {
    // If the email checks out, submit
    if (validateEmail(this.state.email)) {
      // ...
    }
  }

  render () {
    return (
      
        Enter your email:
        
        Sign Up
      
    );
  }
}

// 改进
import React from "react";

const SignupForm = props => {
  const handleSubmit = event => {
    // Needed in case we're sending data to the server XHR-style
    // (but will still work if server-rendered with JS disabled).
    event.preventDefault();

    // Carry on...
  };
  
  return (
    <form method="POST" action="/signup" onSubmit={handleSubmit}>
      <label for="email" class="email-label">Enter your email:</label>
      <input type="email" id="email" required />
      <button>Sign Up</button>
    </form>
  );
};
```

（unsustainable 本意是无法维持/破坏生态的，这里是想说不地道？）

可以看出，改进之后的例子更 accessible，并且代码更少。

A sole preference for JavaScript *will* eventually surface gaps in our understanding of HTML and CSS（对于JS的偏好使用将会最终显现出对于HTML/CSS理解的Gaps）. These knowledge gaps will often result in mistakes we may not even be aware of. 

Frameworks can be useful tools that increase our productivity, but continuing education in core web technologies is essential to creating *usable* experiences, no matter what tools we choose to use. :) 有必要坚持学习core web techs.

## Rely on the web platform and you’ll go far, fast

we’re better *off* （*the subject of frameworks*）when we can rely on established markup patterns and browser features. 毕竟重新发明的轮子/frameworks一定会遇到platform所曾经遭遇的问题（merely *assume* that the author of every JavaScript package we install has solved the problem comprehensively and thoughtfully.）

Carefully consider what you’re building (with React and Redux-like.) and whether a client side router is worth the tradeoffs you’ll inevitably make. Typically, you’re better off without one.

作者说避免使用 client-side rendering：

-   Redux etc会增加100+K的size
-   给出的benchmark中，*client-side rendering 有5.24s的blank，而 server-side rendering没有这个问题* .

使用 rel=prefetch 提前加载资源（可能会带来潜在的浪费）

Service workers 会带来performance显著的提升。当使用 service worker precache route，可以获得与 link prefetching一样的好处。（**然而**）but with a much greater degree of control over requests and responses.

不要使用js布局，JAVASCRIPT ISN’T THE SOLUTION TO YOUR LAYOUT WOES(痛苦)，CSS is designed to do this job.

```css
/* Your mobile-first, non-CSS grid styles goes here */

/* The @supports rule below is ignored by browsers that don't
   support CSS grid, _or_ don't support @supports. */
@supports (display: grid) {
  /* Larger screen layout */
  @media (min-width: 40em) {
    /* Your progressively enhanced grid layout styles go here */
  }
}
```

It’s become clear that the web is *drunk* on JavaScript.

剪枝｜Tree-Shaking：

-   只对ES modules有效，CommonJS不行
-   在build time，不要将ES modules转换成了其他 module format；在@babel/preset-env 中设置modules: false，用于阻止转化成CommonJS

Code-Splitting：

-   deduplicating common code between entry points
-   lazy loading all the functionality you reasonably can with dynamic import()

EXTERNALIZE THIRD-PARTY HOSTED CODE：

简单的说，就是如果已经 &lt;script src="第三方库"/>，可以在 bundler toolchain 中，通过标记 externals，减少重复打包：

```js
// webpack.config.js
module.exports = {
  //...
  externals: {
    lodash: 'lodash'
  }
};
```

## Smaller alternatives for less overhead

| JS Packages | Alternatives   |
| ----------- | -------------- |
| React.js    | Preact.js      |
| moment.js   | date-fns/luxon |
| lodash      | es6+           |
| ...         |                |

## Differentially serve your scripts

简单的来说，就是针对modern/legacy浏览器，分别加载ES6+/ES5的JS code.

```html
<!-- 方案一 -->
<!-- Modern browsers load this file: -->
<script type="module" src="/js/app.mjs"></script>
<!-- Legacy browsers load this file: -->
<script defer nomodule src="/js/app.js"></script>
<!-- ⚠️ IE11/Edge15~18会加载both，¬_¬ -->

<!-- 方案二 -->
<script>
var scriptEl = document.createElement("script");

if ("noModule" in scriptEl) {
  // Set up modern script
  scriptEl.src = "/js/app.mjs";
  scriptEl.type = "module";
} else {
  // Set up legacy script
  scriptEl.src = "/js/app.js";
  scriptEl.defer = true; // type="module" defers by default, so set it here.
}

// Inject!
document.body.appendChild(scriptEl);
</script>
```

## Transpile less

这一部分，我觉得如果一旦使用了babel转译，为了兼容性，必然会导致size explosion。

```js
// es6
function logger(message, level = "log") {
  console[level](message);
}
// babel transpile
function logger(message) {
  var level = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "log";

  console[level](message);
}

// spread 操作更会加大 code 量

// Babel won't touch this
function logger(message, level) {
  console[level || "log"](message);
}
```

## THIRD-PARTY SCRIPTS

### SELF-HOST YOUR THIRD-PARTY SCRIPTS

对于&lt;head>中引用的第三方库，如果将第三方库布置在自己的服务器上，[会减少不少的加载延迟](https://medium.com/caspertechteam/we-shaved-1-7-seconds-off-casper-com-by-self-hosting-optimizely-2704bcbff8ec)，但是需要定期与第三方库做sync；开启http2之后，性能会进一步提升；
