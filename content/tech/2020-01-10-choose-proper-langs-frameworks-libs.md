---
title: "选择合适的LANGs/Frameworks/Libs"
date: "2020-01-10"
---

original ref: https://medium.com/@karti/learn-new-frameworks-after-a-few-years-not-immediately-f8b44dc0ed72

原文给了我一个启示：不必去追逐新的tech。

### Takeaways

>   新的LANGs/Frameworks/Libs，最好等个几年（以Swift1.0为例，2014年发布，2016年可以在产品上使用/production-ready，2018～年之后能成熟使用）,作者原话：waiting for a year or two before learning it, when the tradeoffs are clear.


- 新技术存在不实宣传 (over-hyped) 的问题，可能鼓吹者是很多是developers early in their career。
-   新技术并不如它承诺的那么好，毕竟众口难调嘛。
-   一些框架是有缺陷的，如iCloud丢用户数据的问题。¬_¬
-   如果一开始就使用新框架，如果使用过程中发现不好而切换到别的框架的时候，要花费双倍成本。



### 个人观点

-   技术需要演化的过程，时间会检验最终是成神or跌下神坛（如Hadoop，最初手痒总想练练手，后来看了官方的技术文档，额，¬_¬，还不如当时用的集群SGE/Sun Grid Engine，还好没入坑！！）
-   好马配好鞍，技术需要配相应的toolchain，毕竟是给开发用的，如果DX（developer experience，开发体验，与用户体验User Experience对应）不佳，也没人几个用吧。
-   每个人都有技术偏见，如我比较中意工程化的技术；在三大前端框架中，选择了 React，真心不喜欢 JSP-like；



### Tips

-   开源的库最好选择正式版本的（version > 1）,它的API不会改
-   多个备选项中，选star/使用数多/文档完善的（听说GitHub上有很多KPI项目¬_¬）

____END____

