---
title: "前端优化（2）：优化类原生webviews的JavaScript"
date: "2020-03-31"
---

original ref:

- <https://medium.com/lime-eng/optimizing-js-for-native-like-webviews-f109683e081a>

**Webapp \w Browser or Native？**

> Native 显然有着 Webapp 难以匹敌的性能优势；但是，若论交付速度，Webapp 优势巨大（只需三分钟就能将新特性推给用户，而让 iOS/Android 的用户更新 app，可能需要 2+星期）；Webapp 被人吐糟的重点是页面加载慢/Blanks 时间长，如果将 Webapp 的首页/跳转页打开体验做到接近 Native，岂不快哉！

## 用户流｜ User Flow

1. 用户导航至对应页面
2. app 创建 webview
3. app 将 cookies（包含 auth token，language，location 等）写入 webview
4. webview 从 CDN 加载 HTML 文件
5. 基于用户 language，CDN 运行 VCL 脚本，从 S3 获取 HTML 文件
6. webview 使用 HTML&嵌入的 CSS 渲染 initial screen
7. webview 加载额外依赖：CSS、JS、图片、字体
8. React 运行/更新 DOM
9. JS 使用 cookies 中的 auth token 从服务器中获取数据
10. React 使用获取的数据 re-渲染页面

之后页面就和 native 差不多了

![结构图](https://miro.medium.com/max/1400/0*V7uCZmP8wSZnPRws)

## 首页快速渲染｜ Rendering Initial Screen Quickly

相比于使用 JS 框架渲染首页，静态页面&inline CSS 会 much faster；有两种方案：

1. 服务端渲染｜ SSR/server-side rendering；渲染在服务器中，显然需要扩充服务器的性能（不划算）

2. 静态站点生成器｜ SSG/static site generator；为不同的设备/语言，使用模版生成不同的静态文件

    > SSG 使用 react-render-to-string；
    >
    > 使用 PurgeCSS 嵌入 critical CSS；

    An issue：仍然需要加载 JS，JS 再去加载动态内容；为此，需要在静态 HTML 中写入 placeholder，等待静态内容加载；

## DNS 预取

一次 DNS 查询时间大概在 60-120ms 之间或者更长，提前解析网页中可能的网络连接域名

```html
<link rel="dns-prefetch" href="http://example.com/" />
```

## JS 快速加载｜ Loading JS Quickly

### 更少的 JS 文件

将原先分模块的 JS 文件，交叉合并，以至于每个页面都包括一个 JS 文件；

不过，对于 non-critical 的 JS，仍然作为独立的 chunk

### 更小的 JS 文件

使用 webpack、babel、sass

1. 使用 Google Closure Compiler 压缩代码
2. 使用 Spritesmith & inline SVGs 让一个页面只有一张图片
3. 使用 css-loader 的一个扩展生成短的 class 名
4. 使用[Webpack Bundle Analyzer](https://github.com/webpack-contrib/webpack-bundle-analyzer)
5. 禁用 async/await，因为 polyfills 体积巨大
6. 使用 Webpack’s [optimizationBailout](https://webpack.js.org/plugins/module-concatenation-plugin/#debugging-optimization-bailouts)

### 更快的 JS 文件传输

使用 CDN，开启 HTTP/2，使用 Brotli 压缩（部分浏览器支持，为了兼容性，需要放一个 gzip 版本）

### 非阻塞 JavaScript

```html
<!-- Defer Attribute （如果脚本依赖或依赖于异步脚本）-->
<script defer src="foo.js">

<!-- Async Attribute （如果脚本不依赖于其他脚本）-->
<script async src="foo.js">
```

## CSS 快速加载

### 非阻塞

```html
<link
  rel="preload"
  href="global.min.css"
  as="style"
  onload="this.rel='stylesheet'"
/>
<noscript><link rel="stylesheet" href="global.min.css" /></noscript>
```

### 删除不用的 CSS

有以下工具，不过大多数是静态分析（输入 html + css），这样有一定的局限；推荐 jsx 用 css-in-js 的形式

- 🛠 [UnCSS Online](https://uncss-online.com/)
- 🛠 [PurifyCSS](https://github.com/purifycss/purifycss)
- 🛠 [PurgeCSS](https://github.com/FullHuman/purgecss)

## 字体

### Webfont 格式

在你的网站或者应用使用 WOFF2 格式字体。根据 Google 的说法，WOFF 2.0 Web 字体压缩格式平均比 WOFF 1.0 高 30％的增益。一个较好的做法是使用 WOFF 2.0 作为主要字体，WOFF 1.0 和 TTF 格式字体作为备选。

## 图片

### 响应式图片

```html
<img
  srcset="elva-fairy-480w.jpg 480w, elva-fairy-800w.jpg 800w"
  sizes="(max-width: 600px) 480px,
            800px"
  src="elva-fairy-800w.jpg"
  alt="Elva dressed as a fairy"
/>

<img
  srcset="elva-fairy-320w.jpg, elva-fairy-480w.jpg 1.5x, elva-fairy-640w.jpg 2x"
  src="elva-fairy-640w.jpg"
  alt="Elva dressed as a fairy"
/>

<picture>
  <source media="(max-width: 799px)" srcset="elva-480w-close-portrait.jpg" />
  <source media="(min-width: 800px)" srcset="elva-800w.jpg" />
  <img src="elva-800w.jpg" alt="Chris standing up holding his daughter Elva" />
</picture>
<picture>
  <source type="image/svg+xml" srcset="pyramid.svg" />
  <source type="image/webp" srcset="pyramid.webp" />
  <img
    src="pyramid.png"
    alt="regular pyramid built from four equilateral triangles"
  />
</picture>
```
