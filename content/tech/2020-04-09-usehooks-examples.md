---
title: "useHooks 实例"
date: "2020-04-09"
update: "2020-07-14"
---

original ref:

- <https://usehooks.com>
- <https://joshwcomeau.com/react/the-perils-of-rehydration/>
- <https://www.bitnative.com/2020/07/06/four-ways-to-fetch-data-in-react/>

### useCallback

```js
// 返回一个 memoized 回调函数。
// useCallback(fn, deps) 相当于 useMemo(() => fn, deps)。
const memoizedCallback = useCallback(() => {
  doSomething(a, b);
}, [a, b]);

// 给了我一点启示：
// - 之前一直困扰于用 react 实现顶部导航栏 Nav 根据内容滑动呈现不同的样式
// - 现在使用如下方式应该可以实现
function MeasureExample() {
  const [height, setHeight] = useState(0);
  // 作为 h1 的DOM ref，node 指代这个 DOM Element 对象
  const measuredRef = useCallback((node) => {
    if (node !== null) {
      setHeight(node.getBoundingClientRect().height);
    }
  }, []);

  return (
    <>
      <h1 ref={measuredRef}>Hello, world</h1>
      <h2>The above header is {Math.round(height)}px tall</h2>
    </>
  );
}
```

### useMemo

**可以把 `useMemo` 作为性能优化的手段，但不要把它当成语义上的保证**

```js
// 返回一个 memoized 值。
const memoizedValue = useMemo(() => computeExpensiveValue(a, b), [a, b]);
```

### useRef

类似实例变量的东西，本质上，`useRef` 就像是可以在其 `.current` 属性中保存一个可变值的“盒子”，不仅可以用于 DOM refs。而 `useRef()` 和自建一个 `{current: ...}` 对象的唯一区别是，`useRef` 会在每次渲染时返回同一个 ref 对象。

**避免在渲染期间设置 refs —— 这可能会导致意外的行为。相反的，通常你应该在事件处理器和 effects 中修改 refs。**

```js
function TextInputWithFocusButton() {
  const inputEl = useRef(null);
  const onButtonClick = () => {
    // `current` 指向 DOM Element，与 useCallback 中的 MeasureExample 异曲同工
    inputEl.current.focus();
  };
  return (
    <>
      <input ref={inputEl} type="text" />
      <button onClick={onButtonClick}>Focus the input</button>
    </>
  );
}
```

### useImperativeHandle

这个 API 相较于 useRef，返回的是子组件定义的 props，而不是 DOM Element 实例；

官方推荐这种方法
(**rarely used**)

```js
const MyInput = React.forwardRef((props, ref) => {
  const [val, setVal] = React.useState("");
  const inputRef = React.useRef();

  React.useImperativeHandle(ref, () => ({
    blur: () => {
      document.title = val;
      inputRef.current.blur();
    },
  }));

  return (
    <input
      ref={inputRef}
      val={val}
      onChange={(e) => setVal(e.target.value)}
      {...props}
    />
  );
});

const App = () => {
  const ref = React.useRef(null);
  const onBlur = () => {
    console.log(ref.current); // Only contains one property!
    ref.current.blur();
  };

  return <MyInput ref={ref} onBlur={onBlur} />;
};
```

### useHasMounted

```js
function useHasMounted() {
  const [hasMounted, setHasMounted] = React.useState(false);
  React.useEffect(() => {
    setHasMounted(true);
  }, []);
  return hasMounted;
}
```

以登录状态的 SSR/rehydration 为例，有三个状态：

1. SSR 的时候 null
2. initial 渲染的时候 \<NotLogin />
3. \<Login />

使用 client side 的 render 的时候，没有 bug；因为 render 会根据 props&state 更新 DOM

但是使用 hydrate 的时候，会有 bug；因为 hydrate 会假定 DOM 在 SSR 之后不会改变（React assumes that the DOM won't change. It's just trying to adopt the existing DOM.）

```js
function Navigation() {
  const hasMounted = useHasMounted();
  if (!hasMounted) {
    return null;
  }
  const user = getUser();
  if (user) {
    return <AuthenticatedNav user={user} />;
  }
  return (
    <nav>
      <a href="/login">Login</a>
    </nav>
  );
}
```

### useFetch

这个 implementation 质量不算太高，？？两个 useRef 看不懂

```js
import { useState, useEffect, useRef } from "react";
// This custom hook centralizes and streamlines handling of HTTP calls
export default function useFetch(url, init) {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const prevInit = useRef();
  const prevUrl = useRef();

  useEffect(() => {
    // Only refetch if url or init params change.
    // ??? 为什么要多此一举
    if (prevUrl.current === url && prevInit.current === init) return;
    prevUrl.current = url;
    prevInit.current = init;
    fetch(process.env.REACT_APP_API_BASE_URL + url, init)
      .then((response) => {
        if (response.ok) return response.json();
        setError(response);
      })
      .then((data) => setData(data))
      .catch((err) => {
        console.error(err);
        setError(err);
      })
      .finally(() => setLoading(false));
  }, [init, url]);

  return { data, loading, error };
}
```
