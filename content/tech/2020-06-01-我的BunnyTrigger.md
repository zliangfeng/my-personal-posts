---
title: "我的BunnyTrigger"
date: "2020-06-02"
---

original ref:

- Bunny Triggers <https://www.zhihu.com/question/33634376/answer/125936478>

### html 转 PDF 分页问题

工具：wkhtmltopdf

用过的都知道，如果一个页面放不下，会把剩下的页面放在下一页。这样的话，可能会只把表头留在前一页，甚者把一行文字截取掉下半行。而要求是，内容要输入为美观的可供打印的版本；

方案：1、将需要分页的元素#preview 先渲染出来；2、设定一个 A4 大小为一个 Page，向下滑动 Page，在所有的#preview >div.row 元素中，如果 div 中的 p tag 的定位超出了 Page，判定该 p tag 是需要分页(移动到下一个 Page)；3、对 div 中的 table[0] 元素采用同样的方法判定 thead 是否需要分页；

注意：#preview 元素的结构有一定的特殊性，一个 #preview 只会在页面最后出现 table；所以方案#3 才会无 bug

```js
// reactjs_v15.1.0
componentDidMount: function () {
  var self = this;

  // 设置 timeout 非常关键,
  // 在 componentDidMount 执行之初,可能由于间隔太短, element 还未完全加载到 dom,
  // 导致计算 element 的位置出现了错误
  setTimeout(function () {

    var _page_start = 267; // head
    var _page_offset_break_line = 990; // offset to create a new page for div.row
    var _page_height = 1040; // body
    var _page = 1;

    $("#preview >div.row").each(function () {
      var top = self.getElementViewTop(this);
      var bottom = top + this.offsetHeight;

      if (top > _page_start + _page_offset_break_line) {
        _page++;
        _page_start = top;
      }
      $(this).addClass(`page page-${_page}`);

      if (bottom > _page_start + _page_height) {
        var page_break_div = $(this);
        page_break_div.find('.col-head').each(function () {
          $(this).addClass(`page page-${_page}`);
        });

        var _is_page_break_line_found = false;
        // 在 p tag 中寻找分页行
        page_break_div.find('p').each(function (index) {
          var _top = self.getElementViewTop(this);
          var _bottom = _top + this.scrollHeight;
          _is_page_break_line_found = (_bottom > _page_start + _page_height);
          // found it
          if (_is_page_break_line_found) {
            _page++;
            $(this).addClass(`page page-${_page}`);
            page_break_div.addClass(`page page-${_page}`); // 不需要 heads, 因为不需要在新的页面显示
            _is_page_break_line_found = false;
            // reset, 将 _page_start 重置为 page_break_element_p 的 top;
            _page_start = _top + 10;
          } else {
            $(this).addClass(`page page-${_page}`);
          }
        });
        // 在 table 中寻找分页行
        var table = page_break_div.find('table').get(0);
        if (table != null) {
          var thead = $(table).children('thead').get(0);
          $(table).children("tbody").find("tr").each(function (index) {
            var _bottom = self.getElementViewTop(this) + this.scrollHeight;
            _is_page_break_line_found = ( _bottom > _page_start + _page_height);

            // found it
            if (_is_page_break_line_found) {
              _page++;
              page_break_div.addClass(`page page-${_page}`);
              $(thead).addClass(`page page-${_page}`);
              $(this).addClass(`page page-${_page}`);
              _is_page_break_line_found = false;
              // reset, 将 _page_start 重置为 page_break_element_tr 的 bottom, 因为新页面的 thead 占一行;
              _page_start = _bottom - 92;
            } else {
              if (index == 0)
                $(thead).addClass(`page page-${_page}`);
              $(this).addClass(`page page-${_page}`);
            }
          });
        }
      }
    });

    // rest of code
    ...

  }, 500);


}
```

结语：刚入公司的第一个项目就是实现报告自动化的问题；当时公司出报告是用 word，把里面的文字/表格根据测序分析的结果改一改，一份报告就需要 2 ～ 3 个小时，复查难度大；这个临时方案很有 bunny trigger 的味道，但是由于项目弃坑，就没有再维护；项目可以优化的点大概就是交互部分，替换掉 jquery，至于核心分页的思想，目前没到想更好的，也许用 LaTeX 可以实现完美分页；

### web 系统单机移植

背景：典型的 react+express+mysql 项目，突然接到甲方的要求：系统需要离线部署；

虽然可以在离线服务器上安装 DB，不过毕竟不能 ssh 到甲方服务器上部署，来回折腾肯定少不了；

如果把 DB 换成 in-memory db，集成到 websvr + webapi 的 node app 中，再 webpack 打包成一个可执行文件，那离线部署 = 一键部署，搞定！

由于有一个现成的 in-memory db：json-server，平时用来做 mock rest api，就拿这个动手改造吧！

```js
import path from "path";
import express from "express";
import bodyParser from "body-parser";
import { create, router, defaults } from "json-server";
import createDishCenter from "@bit/create-dish-center"; // 自定义函数
import echo from "./routes/echo";

const server = create();
const middlewares = defaults();

const data_router = router(path.join(__dirname, "db.json")); // in-memory db
const func_router = express.Router();

export const DishCenter = createDishCenter(data_router.db);

func_router.route("/echo").get(echo);
// other routes

server.use(express.static(path.resolve(__dirname, "..", "build")));
server.use(middlewares);
server.use("/data", data_router);

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use((req, res, next) => {
  req.DishCenter = DishCenter;
  // data_router.db 是 json-server 的实例，可以用于在 next() 请求中对 in-memory db 作 crud 操作；
  // eg: const is_dish_name_exist = db.get("dishes").find({ name: dish.name }).value() !== undefined;
  req.db = data_router.db;
  next();
});
server.use("/api", func_router);

server.listen(80, () => {
  console.log("JSON Server is running");
});
```

### 寻找波峰

> dsDNA 是由 ATCG 四种碱基构成的双螺旋结构。分子实验中，可以设计一段单链片段 DNA，与未知的 DNA 片段去结合形成一个 DNA 双链片段。当加热结合有染料的双链 DNA 时，其达到特定的温度/熔点 (Tm) 后，由于 DNA 链的解离和染料的释放，检测的荧光强度 F 会出现突然下降。由此可以绘制荧光强度 F 与温度 T 变化图 a，及 -ΔF/ΔT (荧光变化/温度变化) 与温度图 b，清晰显示熔解曲线动态范围 ；在 Tm 处，图 b 会有一个波峰；

简单来说，就是给定一条曲线，找到 peaks；

如果是标准的曲线，那么 findPeak() 一点都不难写：在 x 轴为 Tm 处，判定左侧连续 m 个上升、右侧连续 n 个下降，即为 peak；

而真实的情况：

1. 仪器存在误差，peak 不会恰好在 Tm 点，而是落在一个范围；解决方案：在参考 Tm 正负 3 摄氏度附近如果有峰值，都算作 peak
2. 样品、试剂存在特异性，真实的 peak 有时存在不明显的连续上升/下降（count_down / count_up < 1 ），与实验同事确认，确实就是真峰；这样的假阴性结果（实验同事称之为抖动）处理起来就很棘手，苦索之后，解决方案：真实峰值的形态都是类似的，其 y 值相对于 y_min 到 y_max，会落在一个范围内（或者说比值 r 在一个范围内）；可以先求得候选峰值附近几个点的 y 的均值，算出比值 r，观察是否落在预设的范围内，如果是，则为真峰；

改进之后的 findPeak()，准确率可以达到 99%；

一点思考：

- 其实只需根据真实情况 2 的方案，就能找出峰值；
- 需要改进的地方：人工复核之后，可以统计一下比值，得到一个无限接近真实情况的范围；
- 即使经过改进，准确率也不可能达到 100%，现实比理论复杂；
