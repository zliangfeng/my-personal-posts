---
title: "折腾iOS Coding"
date: "2021-01-30"
---


## 背景

- 新公司电脑一个bit都拷不出来；
- 虽然可以用自己的电脑查资料，记笔记，但毕竟多台电脑操作不方便；折中的方案是在手机里面记笔记；
- 工作的时候，随手在公司电脑里面记录笔记，现在想同步到GitHub上；

## 实践

### 安装 iOS github client

- 官方 app 就算了，代码都没法拉到本地
- 对比之下，Working Copy 就好多了，可以说是集成了git的文本编辑器
  - 问题是，这个 app 的 git push 是付费的；我不是重度移动coder，不想花这个钱；
下面是解决 git push 的问题

### 安装 git client

- 找了一圈，发现大多数可用的 app 都是 push 要钱，看来此路不通；
- 下一个思路是装个 iOS SHELL 加上 git cli，搜到了一些 iOS terminal，但大多数是 SSH + SFTP之流；
- 直到发现 iSH 的存在

### iSH

- 可以把 iSH 理解成一套只有 terminal 的linux（正合我意，我对linux桌面也不感冒）
  - 可贵的是，iSH 可以用 `apk add "package"` 的方式添加Linux里面主流的包（iSH 底层 AlpineLinux）
- 剩下的，就是行云流水的一套：
  - 装 vim/git/openssh
  - 装 bash（自带的 ash）
  - ssh 连接到 iOS 改 profile（手机的小键盘效率肯定比不了 Macbook）
  - 设置 git config，ssh-keygen 生成 pub key 放到 GitHub；
  - iSH 读取 Working Copy 拉取在本地的 repo；
  - 试用 git cli
- git 一顿操作下：
  - git clone 可用
  - git status 卡，但可用
  - git commit 卡，不可用
  - git push 可用 （**我就要这个功能就OK了啊**）

## Wrapping Up

- iSH 只做 git push，Working Copy 干其他所有，和谐。

剩下的找一个好用的代码编辑器App，那是后话了；
生命不息，折腾不止；
