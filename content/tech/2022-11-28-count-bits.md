---
title: "[算法]计算正整型二进制中 1 的个数"
date: "2022-11-28"
---

### original ref

- <http://www.robalni.org/posts/20220428-counting-set-bits-in-an-interesting-way.txt>

### content

> x = remainning * 2  + 个数位，在此公式的基础上做循环，个数位的和即等于 bit 位为 1 的计数值；
> 所以这转化成求 x - count(remainning) 的问题

```c
unsigned popcnt(unsigned x) {
  unsigned diff = x;
  while (x) {
    x >>= 1;
    diff -= x;
  }
  return diff;
}

// equal to
unsigned popcnt(unsigned x) {
  unsigned sum = 0;
  unsigned remaining = x;
  while (remaining) {
    remaining >>= 1;
    sum += remaining;
  }
  return x - sum;
}
```
