---
title: "[FrontEnd] 复合组件（Compound Components）"
date: "2022-12-02"
tags: ["react components"]
series: ["react advance"]
categories: ["react"]
---

## refs

- <https://epicreact.dev/soul-crushing-components/>
- <https://www.smashingmagazine.com/2021/08/compound-components-react/>

## contents

- 用过 antd modal 组件的，对下面代码一定会很熟悉：

``` js
<Modal
  modalTitle="Register"
  modalLabelText="Registration form"
  openButtonText="Register"
  dismissable={true}
  contents={
    <LoginForm
      onSubmit={register}
      submitButton={<Button variant="secondary">Register</Button>}
    />
  }
/>
```

对于这样的巨石组件，doc 里面的 N 多的参数说明，直接新手劝退ING；

- 改成复合组件：

```js
<Modal>
  <ModalOpenButton>
    <Button variant="secondary">Register</Button>
  </ModalOpenButton>
  <ModalContents aria-label="Registration form">
    <ModalDismissButton>
      <CircleButton>
        <VisuallyHidden>Close</VisuallyHidden>
        <span aria-hidden>×</span>
      </CircleButton>
    </ModalDismissButton>
    <ModalTitle>Register</ModalTitle>
    <LoginForm
      onSubmit={register}
      submitButton={<Button variant="secondary">Register</Button>}
    />
  </ModalContents>
</Modal>
```

逻辑清晰，一目了然；

- 从这里可以看出，采用复合组件的方式，有如下好处
  - 关注点分离
  - 降低复杂度
- 至于坏处，主要是父子组件通讯的问题，推荐使用 `React.createContext` API (React) 或者 `provide/inject` (Vue)
