# vscode 快捷键练习

## Code navigation

💡 See Keybindings help to see all defined shortcuts and their documentation.
Key VSCode Command

- [x] = / == editor.action.formatSelection
- [ ] gh / K editor.action.showHover
- [x] gd / C-] editor.action.revealDefinition
- [ ] gf editor.action.revealDeclaration
- [ ] gH editor.action.referenceSearch.trigger
- [ ] gO workbench.action.gotoSymbol
- [ ] C-w gd / C-w gf editor.action.revealDefinitionAside
- [ ] gD editor.action.peekDefinition
- [ ] gF editor.action.peekDeclaration
- [ ] Tab togglePeekWidgetFocus Switch between peek editor and reference list.
- [ ] C-n / C-p Navigate lists, parameter hints, suggestions, quick-open, cmdline history, peek reference list

💡 To specify the default peek mode, modify editor.peekWidgetDefaultFocus in your settings.

## Explorer/list navigation

💡 See Keybindings help to see all defined shortcuts and their documentation.
Key VSCode Command

- [x] j or k list.focusDown/Up
- [x] h or l list.collapse/select
- [x] Enter list.select
- [x] gg list.focusFirst
- [x] G list.focusLast
- [x] o list.toggleExpand
- [x] C-u or C-d list.focusPageUp/Down
- [x] zo or zO list.expand
- [x] zc list.collapse
- [x] zC list.collapseAllToFocus
- [x] za or zA list.toggleExpand
- [x] zm or zM list.collapseAll
- [x] /  or Escape list.toggleKeyboardNavigation

## Explorer file manipulation

💡 See Keybindings help to see all defined shortcuts and their documentation.
Key VSCode Command

- [x] r renameFile
- [x] d deleteFile
- [x] y filesExplorer.copy
- [x] x filesExplorer.cut
- [x] p filesExplorer.paste
- [x] v explorer.openToSide
- [x] a explorer.newFile
- [x] A explorer.newFolder
